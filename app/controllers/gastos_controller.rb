class GastosController < ApplicationController
  before_action :set_gasto, only: %i[ show edit update destroy]

  # GET /gastos or /gastos.json
  def index
    @gastos = Gasto.all
  end

  # GET /gastos/1 or /gastos/1.json
  def show
  end

  # GET /gastos/new
  def new
    @gasto = Gasto.new
  end

  # GET /gastos/1/edit
  def edit
  end

  # POST /gastos or /gastos.json
  def create
    @gasto = Gasto.new(gasto_params)

    respond_to do |format|
      if @gasto.save
        format.html { redirect_to gasto_url(@gasto), notice: "Gasto was successfully created." }
        format.json { render :show, status: :created, location: @gasto }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @gasto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /gastos/1 or /gastos/1.json
  def update
    respond_to do |format|
      if @gasto.update(gasto_params)
        format.html { redirect_to classificar_path, notice: "Gasto was successfully updated." }
        format.json { render :show, status: :ok, location: @gasto }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @gasto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gastos/1 or /gastos/1.json
  def destroy
    @gasto.destroy!

    respond_to do |format|
      format.html { redirect_to gastos_url, notice: "Gasto was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def importfile
    if params[:extrato] != nil
        extrato = params[:extrato]

        doc = File.open(extrato)

        tipo = ""
        data = ""
        valor = ""
        obs = ""

        doc.each do |linha|

          if linha.include? "<TRNTYPE>"
              tipo = linha.strip
              tipo = tipo.delete_prefix "<TRNTYPE>"
              puts tipo
          end

          if linha.include? "<DTPOSTED>"
              data = linha.strip
              data = data.delete_prefix "<DTPOSTED>"
              data = data[6,2]+"/"+data[4,2]+"/"+data[0,4]
              puts data
          end

          if linha.include? "<TRNAMT>"
              valor = linha.strip
              valor = valor.delete_prefix "<TRNAMT>"
              puts valor
          end

          if linha.include? "<MEMO>"
              obs = linha.strip
              obs = obs.delete_prefix "<MEMO>"
              puts obs
          end

          if tipo != "" and data != "" and valor != "" and obs != ""

              @gastos = Gasto.where(data: data).where(valor: valor)

              if @gastos.count == 0
                @g = Gasto.new
                @g.data = Date.parse data
                @g.valor = valor.to_f
                @g.tipogasto_id = 1
                @g.obs = obs
                if !@g.save
                  puts "não salvou"
                end
              end

              tipo = ""
              data = ""
              valor = ""
              obs = ""

          end

        end
    end
  end

  def classificar
    @gastos = Gasto.where(tipogasto_id: 1)
  end

  def consultames
    @tiposd = Tipogasto.where("transacao = 'D'")
    @tiposc = Tipogasto.where("transacao = 'C'")
    @gastos = Gasto.all
    @receitas = Gasto.where("valor > 0")
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gasto
      @gasto = Gasto.find(params[:id])
      @tipos = Tipogasto.all
    end

    # Only allow a list of trusted parameters through.
    def gasto_params
      params.require(:gasto).permit(:data, :tipogasto_id, :valor, :obs)
    end

end
