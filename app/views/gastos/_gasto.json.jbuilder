json.extract! gasto, :id, :data, :tipogasto_id, :valor, :obs, :created_at, :updated_at
json.url gasto_url(gasto, format: :json)
