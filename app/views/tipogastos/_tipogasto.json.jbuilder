json.extract! tipogasto, :id, :tipo, :created_at, :updated_at
json.url tipogasto_url(tipogasto, format: :json)
