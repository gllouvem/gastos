Rails.application.routes.draw do
  resources :gastos
  resources :tipogastos

  #resources :home
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  get  '/importfile', to: 'gastos#importfile'
  post '/importfile', to: 'gastos#importfile'

  get  '/classificar', to: 'gastos#classificar'
  post '/classificar', to: 'gastos#classificar'

  get  '/consultames', to: 'gastos#consultames'
  post '/consultames', to: 'gastos#consultames'

  get  '/updateclassificar', to: 'gastos#updateclassificar'
  post '/updateclassificar', to: 'gastos#updateclassificar'
  patch '/updateclassificar', to: 'gastos#updateclassificar'


  # Defines the root path route ("/")
  root "home#index"
end
