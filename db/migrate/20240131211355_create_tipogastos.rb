class CreateTipogastos < ActiveRecord::Migration[7.1]
  def change
    create_table :tipogastos do |t|
      t.string :tipo

      t.timestamps
    end
  end
end
