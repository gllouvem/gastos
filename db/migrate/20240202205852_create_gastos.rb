class CreateGastos < ActiveRecord::Migration[7.1]
  def change
    create_table :gastos do |t|
      t.date :data
      t.belongs_to :tipogasto, null: false, foreign_key: true
      t.float :valor
      t.string :obs

      t.timestamps
    end
  end
end
