class AddTransacaoToTipogasto < ActiveRecord::Migration[7.1]
  def change
    add_column :tipogastos, :transacao, :string
  end
end
