require "test_helper"

class TipogastosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipogasto = tipogastos(:one)
  end

  test "should get index" do
    get tipogastos_url
    assert_response :success
  end

  test "should get new" do
    get new_tipogasto_url
    assert_response :success
  end

  test "should create tipogasto" do
    assert_difference("Tipogasto.count") do
      post tipogastos_url, params: { tipogasto: { tipo: @tipogasto.tipo } }
    end

    assert_redirected_to tipogasto_url(Tipogasto.last)
  end

  test "should show tipogasto" do
    get tipogasto_url(@tipogasto)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipogasto_url(@tipogasto)
    assert_response :success
  end

  test "should update tipogasto" do
    patch tipogasto_url(@tipogasto), params: { tipogasto: { tipo: @tipogasto.tipo } }
    assert_redirected_to tipogasto_url(@tipogasto)
  end

  test "should destroy tipogasto" do
    assert_difference("Tipogasto.count", -1) do
      delete tipogasto_url(@tipogasto)
    end

    assert_redirected_to tipogastos_url
  end
end
