require "application_system_test_case"

class GastosTest < ApplicationSystemTestCase
  setup do
    @gasto = gastos(:one)
  end

  test "visiting the index" do
    visit gastos_url
    assert_selector "h1", text: "Gastos"
  end

  test "should create gasto" do
    visit gastos_url
    click_on "New gasto"

    fill_in "Data", with: @gasto.data
    fill_in "Obs", with: @gasto.obs
    fill_in "Tipogasto", with: @gasto.tipogasto_id
    fill_in "Valor", with: @gasto.valor
    click_on "Create Gasto"

    assert_text "Gasto was successfully created"
    click_on "Back"
  end

  test "should update Gasto" do
    visit gasto_url(@gasto)
    click_on "Edit this gasto", match: :first

    fill_in "Data", with: @gasto.data
    fill_in "Obs", with: @gasto.obs
    fill_in "Tipogasto", with: @gasto.tipogasto_id
    fill_in "Valor", with: @gasto.valor
    click_on "Update Gasto"

    assert_text "Gasto was successfully updated"
    click_on "Back"
  end

  test "should destroy Gasto" do
    visit gasto_url(@gasto)
    click_on "Destroy this gasto", match: :first

    assert_text "Gasto was successfully destroyed"
  end
end
