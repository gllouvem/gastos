require "application_system_test_case"

class TipogastosTest < ApplicationSystemTestCase
  setup do
    @tipogasto = tipogastos(:one)
  end

  test "visiting the index" do
    visit tipogastos_url
    assert_selector "h1", text: "Tipogastos"
  end

  test "should create tipogasto" do
    visit tipogastos_url
    click_on "New tipogasto"

    fill_in "Tipo", with: @tipogasto.tipo
    click_on "Create Tipogasto"

    assert_text "Tipogasto was successfully created"
    click_on "Back"
  end

  test "should update Tipogasto" do
    visit tipogasto_url(@tipogasto)
    click_on "Edit this tipogasto", match: :first

    fill_in "Tipo", with: @tipogasto.tipo
    click_on "Update Tipogasto"

    assert_text "Tipogasto was successfully updated"
    click_on "Back"
  end

  test "should destroy Tipogasto" do
    visit tipogasto_url(@tipogasto)
    click_on "Destroy this tipogasto", match: :first

    assert_text "Tipogasto was successfully destroyed"
  end
end
